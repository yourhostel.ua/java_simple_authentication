package project.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConn {
    public static Connection make() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/test",
                "postgres",
                "52486"
        );
    }
}