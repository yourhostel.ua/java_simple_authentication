package project.db;

import project.interfaces.Users;
import project.model.User;
import java.sql.*;
import java.util.Optional;

public class UsersInDB implements Users {
    private final Connection conn;

    public UsersInDB(Connection conn) {
        this.conn = conn;
    }


    public void addUser(User user) throws SQLException {
        PreparedStatement st = conn.prepareStatement(
                """
                    INSERT INTO users (user_id, login, password)
                                 values (?, ?, ?)
                    """
        );
        st.setString(1, user.user_id());
        st.setString(2, user.login());
        st.setString(3, user.password());
        st.execute();
    }

    @Override
    public void set(User user){
        try {
            addUser(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<User> getUser_id(String id_user) throws SQLException {
        PreparedStatement st = conn.prepareStatement(
                """
                    SELECT *
                    FROM users
                    WHERE user_id = ?
                    """
        );
        st.setString(1, id_user);

        ResultSet rs = st.executeQuery();
        if (rs != null && rs.next()) {
            return Optional.of(new User(
                    rs.getString("login"),
                    rs.getString("password"),
                    rs.getString("user_id")
                    ));
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> get_user(String user_id) {
        try {
            return getUser_id(user_id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<User> getUserById(String login, String password) throws SQLException {
        PreparedStatement st = conn.prepareStatement(
                """
                        SELECT *
                        FROM users
                        WHERE login = ? AND password = ?
                        """
        );
        st.setString(1, login);
        st.setString(2, password);
        ResultSet rs = st.executeQuery();

        if (rs != null && rs.next()) {
            return Optional.of(new User(rs.getString("login"),
                    rs.getString("password"),
                    rs.getString("user_id")));
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> get(String login, String password) {
        try {
            return getUserById(login, password);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
