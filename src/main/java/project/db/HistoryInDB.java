package project.db;

import project.interfaces.History;
import project.model.Item;
import project.utils.DbUtils;

import java.sql.*;

public class HistoryInDB implements History {

    private final Connection conn;

    public HistoryInDB(Connection conn) {
        this.conn = conn;
    }

    private void saveToDb(Item item) throws SQLException {
        PreparedStatement st = conn.prepareStatement(
                """
                    INSERT INTO history (x, y, z, t, user_id, cr)
                                 values (?, ?, ?, ?, ?, ?)
                    """
        );
        st.setInt(1, item.x());
        st.setInt(2, item.y());
        st.setInt(3, item.z());
        st.setString(4, item.tmp());
        st.setString(5, item.user_id());
        st.setString(6, item.cr());
        st.execute();
    }

    @Override
    public void put(Item item) {
        try {
            saveToDb(item);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Iterable<Item> getAllFromDb(String user_id) throws Exception {
        PreparedStatement st = conn.prepareStatement(
                """
                    SELECT *
                    FROM history
                    WHERE user_id = ?
                    ORDER BY user_id
                    """
        );
        st.setString(1, user_id);

        ResultSet rs0 = st.executeQuery();
        return DbUtils.convert(rs0, rs -> {
            int x = rs.getInt("x");
            String cr = rs.getString("cr");
            int y = rs.getInt("y");
            int z = rs.getInt("z");
            String t = rs.getString("t");
            String user0_id = rs.getString("user_id");

            return Item.of(x, cr, y, z, t, user0_id);
        });
    }

    @Override
    public Iterable<Item> getAll(String user_id) {
        try {
            return getAllFromDb(user_id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
