package project.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import project.model.Item;
import project.utils.Auth;
import project.interfaces.History;
import project.utils.ConfigFM;


public class CalculatorServlet extends HttpServlet {

    private final History history;
    private final Configuration cfg;

    public CalculatorServlet(History history) throws IOException {
        this.history = history;
        this.cfg = ConfigFM.make("calculator");
    }

    private HashMap<String, Object> tpl() throws IOException {
        HashMap<String, Object> data = new HashMap<>();
        data.put("button_a", "<a href=\"/logout\">Sign out</a>");
        data.put("button_b", "<a href=\"/history\">History</a>");
        data.put("history", "");
        data.put("items", new ArrayList<>());
        data.put("main", cfg.getTemplate("setCalc.ftl"));
        return data;
    }

    @SneakyThrows
    private void redirect(HttpServletResponse resp, String to) {
        resp.sendRedirect(to);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            HashMap<String, Object> data = tpl();
            Auth.tryGetCookie(req)
                    .orElseThrow(() -> new IllegalArgumentException("user not registered"));
            try (PrintWriter w = resp.getWriter()) {
                cfg
                        .getTemplate("home.ftl")
                        .process(data, w);
            } catch (TemplateException x) {
                throw new RuntimeException(x);
            }
        } catch (IllegalArgumentException e) {
            redirect(resp, "/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try{
        String cr, user_id; int x, y, z;

        UUID uid = Auth.tryGetCookie(req)
                .orElseThrow(() -> new IllegalArgumentException("user not registered"));

        try (Reader reader = req.getReader()) {
            req.setCharacterEncoding("UTF-8");
            resp.setCharacterEncoding("UTF-8");
            resp.setContentType("application/json; charset=UTF-8");

            StringBuilder body = new StringBuilder();
            char[] buffer = new char[1024]; int readChars;

            while ((readChars = reader.read(buffer)) != -1) {
                body.append(buffer, 0, readChars);
            }

            Item item = new ObjectMapper().readValue(String.valueOf(body), Item.class);

            x = item.x(); cr = item.cr(); y = item.y();
            z = item.z(); user_id = String.valueOf(uid);

            String json = new ObjectMapper()
                    .writeValueAsString(Item.of(x,cr, y, z, user_id));

            resp.getWriter().print(json);

            System.out.println(json);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

//        try {
//            cr = req.getParameter("cr");
//            x = Integer.parseInt(req.getParameter("x"));
//            y = Integer.parseInt(req.getParameter("y"));
//            z = Integer.parseInt(req.getParameter("z"));
//        } catch (NumberFormatException e) {
//            throw new RuntimeException(e);
//        }


        history.put(Item.of(x, cr, y, z, user_id));

//redirect(resp, "/calc");

        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());

        }
    }
}
