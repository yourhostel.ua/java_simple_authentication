package project.servlets;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import project.utils.ResourcesOps;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_32);
        cfg.setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
        cfg.setDirectoryForTemplateLoading(new File(ResourcesOps.dirUnsafe("calculator")));

        HashMap<String, Object> data = new HashMap<>();
        data.put("button_a", "<a href=\"/login\">Sign in</a>");
        data.put("button_b", "<a href=\"/register\">Register</a>");
        data.put("history", "");
        data.put("items", new ArrayList<>());
        data.put("main", "");

        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("home.ftl")
                    .process(data, w);
        } catch (TemplateException x) {
            throw new RuntimeException(x);
        }
    }
}
