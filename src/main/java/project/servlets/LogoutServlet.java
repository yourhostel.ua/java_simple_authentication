package project.servlets;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;

public class LogoutServlet extends HttpServlet {

    @SneakyThrows
    private void redirect(HttpServletResponse resp, String to) {
        resp.sendRedirect(to);
    }

    // http://localhost:8080/logout
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {

        // creating
        Cookie c = new Cookie("UID", "");
        c.setMaxAge(0); // seconds

        // attach
        resp.addCookie(c);
        redirect(resp, "/");
    }
}
