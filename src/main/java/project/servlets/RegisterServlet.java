package project.servlets;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import project.interfaces.Users;
import project.model.User;
import project.utils.ConfigFM;
import project.utils.ResourcesOps;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class RegisterServlet extends HttpServlet {
    private final Users users;

    public RegisterServlet(Users users) {
        this.users = users;
    }

    @SneakyThrows
    private void redirect(HttpServletResponse resp, String to) {
        resp.sendRedirect(to);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Configuration cfg = ConfigFM.make("calculator");

        HashMap<String, Object> data = new HashMap<>();
        data.put("button_a", "<a href=\"/\">Home</a>");
        data.put("button_b", "<a href=\"/login\">Sign in</a>");
        data.put("history", "");
        data.put("items", new ArrayList<>());
        data.put("main", cfg.getTemplate("setForm.ftl"));

        try (PrintWriter w = resp.getWriter()) {
            cfg
                    .getTemplate("home.ftl")
                    .process(data, w);
        } catch (TemplateException x) {
            throw new RuntimeException(x);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        Cookie c = new Cookie("UID", UUID.randomUUID().toString());
        resp.addCookie(c);

        users.set(new User(req.getParameter("username"),
                req.getParameter("password"),
                c.getValue()
                ));
        redirect(resp, "/calc");
    }
}
