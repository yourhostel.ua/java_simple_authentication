package project.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Optional;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import project.interfaces.Users;
import project.model.Item;
import project.model.User;
import project.utils.Auth;
import project.interfaces.History;
import project.utils.ConfigFM;

@RequiredArgsConstructor
public class HistoryServlet extends HttpServlet {
    private final History history;
    private final Users users;
    private final Configuration cfg;

    public HistoryServlet(History history, Users users) throws IOException {
        this.history = history;
        this.cfg = ConfigFM.make("calculator");
        this.users = users;
    }

    @SneakyThrows
    private void redirect(HttpServletResponse resp, String to) {
        resp.sendRedirect(to);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {

            String id = String.valueOf(Auth.tryGetCookie(req)
                    .orElseThrow(() -> new IllegalArgumentException("user not registered")));

            try (PrintWriter w = resp.getWriter()) {
                Optional<User> optional = users.get_user(id);

                if (optional.isPresent()){
                    String userName = optional.get().login();

                    HashMap<String, Object> data = new HashMap<>();
                    data.put("button_a", "<a href=\"/logout\">Sign out</a>");
                    data.put("button_b", "<a href=\"/calc\">Calculator</a>");
                    data.put("main", "");
                    data.put("history", String.format("<div class=\"card-aside\">%s</div>", userName));
                    data.put("items", history.getAll(id));

                    cfg
                            .getTemplate("home.ftl")
                            .process(data, w);
                }
            } catch (TemplateException x) {
                throw new RuntimeException(x);
            }
        } catch (IllegalArgumentException e) {
            redirect(resp, "/");
        }
    }
}