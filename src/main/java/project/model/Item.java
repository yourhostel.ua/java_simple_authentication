package project.model;

import java.sql.Timestamp;

public record Item(int x, String cr, int y, int z, String tmp, String user_id) {

    public static Item of(int x, String cr, int y, int z, String user_id) {
        return new Item(x, cr, y, z, new Timestamp(System.currentTimeMillis()).toString().split("\\.")[0], user_id);
    }

    public static Item of(int x, String cr, int y, int z, String tmp, String user_id) {
        return new Item(x, cr, y, z, tmp, user_id);
    }

    @Override
    public String toString() {
        return String.format("%-30s | %d %s %d = %d", tmp, x, cr, y, z);
    }

}
