package project.interfaces;

import project.model.User;

import java.sql.SQLException;
import java.util.Optional;

public interface Users {
    void set(User user);

    Optional<User> get_user(String user_id);

    Optional<User> get(String login, String password);
}
