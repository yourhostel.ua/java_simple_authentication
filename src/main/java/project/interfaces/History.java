package project.interfaces;

import project.model.Item;

public interface History {

    void put(Item item);

    Iterable<Item> getAll(String id);
}
