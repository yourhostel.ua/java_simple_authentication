package project.interfaces;

public interface FunctionEx<A, B> {
    B apply(A a) throws Exception;
}
