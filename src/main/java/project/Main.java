package project;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import project.db.DbConn;
import project.db.HistoryInDB;
import project.db.UsersInDB;
import project.interfaces.Users;
import project.servlets.*;
import project.interfaces.History;
import project.utils.ResourcesOps;

import java.sql.Connection;

public class Main {

    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        Connection connection = DbConn.make();
        History history = new HistoryInDB(connection);
        Users users = new UsersInDB(connection);

        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new StaticContentServlet(ResourcesOps.dirUnsafe("calculator/static"))), "/calculator/static/*");

        handler.addServlet(new ServletHolder(new HomeServlet()), "/");
        handler.addServlet(new ServletHolder(new LoginServlet(users)), "/login");
        handler.addServlet(new ServletHolder(new RegisterServlet(users)), "/register");
        handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
        handler.addServlet(new ServletHolder(new CalculatorServlet(history)), "/calc");
        handler.addServlet(new ServletHolder(new HistoryServlet(history, users)), "/history");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}