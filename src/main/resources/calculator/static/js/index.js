'use strict';
((D, B, log = arg => console.log(arg)) => {
    let sign = '', mr = 0, a = 0, sign2 = '', k = D.querySelector('[type=text]');
    let x = 0, cr = '', y = 0, z = 0;
    const collect = (a, char, b, c) => {x = a; cr = char; y = b; z = c;},
        mAct = {
            '+':(a, b) => { k.value = a + b; collect(a, '+', b, k.value)},
            '-':(a, b) => { k.value = a - b; collect(a, '-', b, k.value)},
            '*':(a, b) => { k.value = a * b; collect(a, '*', b, k.value)},
            '/':(a, b) => { k.value = a / b; collect(a, '/', b, k.value)},
            'mrc':() => { (mr !== 0 && k.value === '0') ? k.value = mr : mr = 0 },  //якщо на екрані 0 повертає пам'ять на екран інакше стирає
            'm-':() => { mr = +mr - +k.value },      //віднімає від пам'яті
            'm+':() => { mr = +mr + +k.value },      //додає до пам'яті
        },
        write = e =>{ let l = k.value; k.value = `${l}${e.target.value}` },
        concat = (e) => {
            if(e.target.value !== undefined && e.target.value.match(/^\d|[.]$/)){
                if(k.value === '0' && e.target.value === '.'){ k.value = `${k.value}${e.target.value}` }
                if(k.value === '0'){ k.value = '' }
                if (!k.value.includes('.') && e.target.value === '.'){ write(e) }
                if(e.target.value !== '.'){ write(e) }
            }
        },
        swap = e => { if(e.target.value !== undefined && e.target.value.match(/^\d|[.]$/) && sign !== ''){ k.value = ''; sign2 = sign; sign = '' } },
        clean = e => { if (e.target.value === 'C'){ k.value = '0' } },
        read = e => { if (e.target.value !== undefined && !(e.target.value.match(/^\d|[.]$/)) && e.target.value !== 'C'){ sign = e.target.value; a = k.value; } },
        result = e => { if (sign2 !== undefined && sign2 !== '' && e.target.value === '=' && a !== 0 ){ mAct[sign2](+a, +k.value) } },
        memorize = e => { if (e.target.value !== undefined && e.target.value.match(/^mrc|m-|m\+$/)){ mAct[e.target.value]() } },
        m = () => {(mr === 0) ? D.querySelector('.mr').textContent = '' : D.querySelector('.mr').textContent = 'm'},

        emulateSubmit =e=>{if (e.target.value === '=' && cr !== ""){

                                 async function postData(url = '', params = {}) {
                                     // Default options are marked with *
                                     const response = await fetch(url, {
                                         method: 'POST', // *GET, POST, PUT, DELETE, etc.
                                         mode: 'cors', // no-cors, *cors, same-origin
                                         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                                         credentials: 'same-origin', // include, *same-origin, omit
                                         headers: {
                                             'Content-Type': 'application/json'
                                             // 'Content-Type': 'application/x-www-form-urlencoded',
                                         },
                                         redirect: 'follow', // manual, *follow, error
                                         referrerPolicy: 'no-referrer', // no-referrer, *client
                                         body: JSON.stringify(params) // body data type must match "Content-Type" header
                                     });
                                     return await response.json(); // parses JSON response into native JavaScript objects
                                 }


                          try {
                             let params = {
                                 "x": x.toString(),
                                 "y": y.toString(),
                                 "cr": cr,
                                 "z": z,
                             };
//                              fetch('http://localhost:8080/calc', {
//                                  method: 'POST',
//                                  headers: {"Content-Type": "application/json"},
//                                  body: JSON.stringify(params)
//                              })
//                                  .then(response =>{
//
//                                      console.log('Успіх: response.ok', JSON.stringify(response.json()));
//                                  })

                             postData('http://localhost:8080/calc', params)
                                 .then(data => {
                                     console.log(data); // JSON data parsed by `response.json()` call
                                 });


                          } catch (error) {
                              console.error('Помилка:', error);
                          }


         //            const params = new FormData();
         //            params.append("x", x + "");
         //            params.append("cr", cr + "");
         //            params.append("y", y + "");
         //            params.append("z", z + "");
         //            const form = document.createElement("form");
         //            form.setAttribute("method", "POST");
         //            for(const param of params) {
         //                    let hiddenField = document.createElement("input");
         //                    hiddenField.setAttribute("type", "hidden");
         //                    hiddenField.setAttribute("name", param[0] + "");
         //                    hiddenField.setAttribute("value", param[1] + "");
         //                    form.append(hiddenField);
         //                }
         //            document.body.appendChild(form);
         //            form.submit();
        }};

const boxCalc = D.querySelector('.box')
if(boxCalc !== null){
    boxCalc.addEventListener('click',(e)=>{
        result(e); memorize(e); swap(e); clean(e); read(e); concat(e); m(e); emulateSubmit(e)
    })
}


    log('the end')

})(document, document.body);