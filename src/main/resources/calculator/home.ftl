<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>step-project-cards</title>
    <link rel="stylesheet" href="calculator/static/style/style.css"/>
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    <header class="header">
        <nav>
            <ul class="list">
                <li class="logo"><a href="#"><img src="" alt=""></a></li>
                <li class="item">
                    <p class="enter btn item">${button_a}</p>
                    <p class="exit btn item">${button_b}</p>
                </li>
            </ul>
        </nav>
    </header>
    <aside class="aside">
        <div class="form">${history}</div>
        <div class="result-search">
<div class="card-aside">
<#list items as line>
<div class="history-box"><p class="date">${line.tmp()}</p><p class="operation"> ${line.x()} ${line.cr()} ${line.y()} = ${line.z()}</p></div>
<hr>
<#else>
</#list>
</div>
        </div>
    </aside>
    <main class="main">
         ${main}
    </main>
</div>
<!--<script src="calculator/static/js/drag-and-drop.js"></script>-->
<script src="calculator/static/js/index.js"></script>
</body>
</html>

<!---->